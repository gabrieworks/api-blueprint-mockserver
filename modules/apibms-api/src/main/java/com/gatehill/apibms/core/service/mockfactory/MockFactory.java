/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.service.mockfactory;

import com.gatehill.apibms.core.exception.ServiceException;
import com.gatehill.apibms.core.model.MockDefinition;

import java.io.File;

/**
 * Created by pete on 15/02/2014.
 */
public interface MockFactory {
    public enum BlueprintFormat {
        MARKDOWN,
        AST_JSON,
        AST_YAML
    }

    /**
     * Build a {@link com.gatehill.apibms.core.model.MockDefinition} from an API Blueprint file.
     *
     * @param blueprintFile
     * @param format
     * @return
     * @throws ServiceException
     */
    MockDefinition createMock(File blueprintFile, BlueprintFormat format) throws ServiceException;
}
