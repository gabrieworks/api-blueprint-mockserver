/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.service;

import com.gatehill.apibms.core.model.MockServer;
import com.gatehill.apibms.core.model.MockDefinition;

/**
 * Represents a protocol- and implementation-agnostic server that can handle requests for a given mock and respond
 * according to the specified MockDefinition.
 *
 * @author pete
 */
public interface ServerService {
    String HOST_LOCALHOST = "localhost";

    /**
     * Start a server for the specified MockDefinition on {@link #HOST_LOCALHOST} on a free port.
     *
     * @param mock the MockDefinition describing the server behaviour
     * @return a MockServer describing the running server
     */
    MockServer start(MockDefinition mock);

    /**
     * Start a server for the specified MockDefinition on {@link #HOST_LOCALHOST} and the given <code>port</code>.
     *
     * @param mock the MockDefinition describing the server behaviour
     * @param port the port to listen on
     * @return a MockServer describing the running server
     */
    MockServer start(MockDefinition mock, int port);

    /**
     * Start a server for the specified MockDefinition on the given <code>host</code> and <code>port</code>.
     *
     * @param mock the MockDefinition describing the server behaviour
     * @param host the host to bind to
     * @param port the port to listen on
     * @return a MockServer describing the running server
     */
    MockServer start(MockDefinition mock, String host, int port);

    /**
     * Stop the server described by the specified MockServer.
     *
     * @param server the server to stop
     */
    void stop(MockServer server);
}
