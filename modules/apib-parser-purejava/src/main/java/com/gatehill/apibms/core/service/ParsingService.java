/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.service;

import com.gatehill.apibms.core.exception.ServiceException;
import org.w3c.dom.Document;

import java.io.File;
import java.io.InputStream;

/**
 * Created by pete on 15/02/2014.
 */
public interface ParsingService {
    /**
     * Convert a Markdown format file to HTML.
     *
     * @param markdownFile
     * @return
     * @throws ServiceException
     */
    String parseMarkdownFile(File markdownFile) throws ServiceException;

    /**
     * Convert a Markdown format {@link java.io.InputStream} to HTML.
     *
     * @param markdownStream
     * @return
     */
    String parseMarkdown(InputStream markdownStream);

    /**
     * Convert an HTML {@link String} to an XHTML {@link org.w3c.dom.Document}.
     *
     * @param html
     * @return
     * @throws ServiceException
     */
    Document toXhtmlDocument(String html) throws ServiceException;
}
