/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core;

import com.gatehill.apib.parser.service.*;
import com.gatehill.apib.parser.service.v2.JsonAstParserServiceImpl;
import com.gatehill.apib.parser.service.v2.YamlAstParserServiceImpl;
import com.gatehill.apibms.core.service.*;
import com.gatehill.apibms.core.service.mockfactory.MockFactory;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Singleton;

/**
 * Created with IntelliJ IDEA.
 * User: pete
 * Date: 23/02/2014
 * Time: 11:36
 * To change this template use File | Settings | File Templates.
 */
public class BootstrapModule implements Module {
    private final Class<? extends MockFactory> mockFactoryClass;
    private final MockFactory.BlueprintFormat format;

    public BootstrapModule(MockFactory.BlueprintFormat format, Class<? extends MockFactory> mockFactoryClass) {
        assert format != null;
        this.format = format;

        assert mockFactoryClass != null;
        this.mockFactoryClass = mockFactoryClass;
    }

    @Override
    public void configure(Binder binder) {
        binder.bind(MockFactory.class).to(mockFactoryClass).in(Singleton.class);
        binder.bind(ServerService.class).to(ServerServiceImpl.class).in(Singleton.class);
        binder.bind(ExecutionService.class).to(ExecutionServiceImpl.class).in(Singleton.class);
        binder.bind(SettingsService.class).to(SettingsServiceImpl.class).in(Singleton.class);

        // parsers
        binder.bind(ParsingService.class).to(ParsingServiceImpl.class).in(Singleton.class);
        binder.bind(BlueprintParserService.class).to(SnowcrashBlueprintParserServiceImpl.class).in(Singleton.class);

        switch (format) {
            case MARKDOWN:
                // MD gets converted to JSON
            case AST_JSON:
                binder.bind(AstParserService.class).to(JsonAstParserServiceImpl.class).in(Singleton.class);
                break;

            case AST_YAML:
                binder.bind(AstParserService.class).to(YamlAstParserServiceImpl.class).in(Singleton.class);
                break;

            default:
                throw new UnsupportedOperationException("Unsupported format: " + format);
        }
    }
}
