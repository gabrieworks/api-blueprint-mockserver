/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.service.responsematcher;

import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import com.gatehill.apibms.core.model.ResponseDefinition;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HeaderValues;
import io.undertow.util.Headers;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Try to identify the most sensible response using the 'Accept' and 'Content-Type' headers.
 */
public class ContentNegotiationResponseMatcher implements ResponseMatcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContentNegotiationResponseMatcher.class);

    @Override
    public Map<Integer, ResponseDefinition> matchResponse(HttpServerExchange exchange, ResourceDefinition endpoint, RequestDefinition request, Map<Integer, ResponseDefinition> candidates) {
        final Map<Integer, ResponseDefinition> matched = new HashMap<>();

        final HeaderValues acceptHeaders = exchange.getRequestHeaders().get(Headers.ACCEPT);

        if (null != acceptHeaders && acceptHeaders.size() > 0) {
            final String acceptValue = acceptHeaders.get(0);

            if (StringUtils.isNotBlank(acceptValue)) {
                for (ResponseDefinition response : candidates.values()) {
                    if (acceptValue.equals(response.getHeaders().get(Headers.CONTENT_TYPE_STRING))) {
                        LOGGER.trace("Match response {} to request with path '{}' using Accept and Content-Type headers (Accept: {})",
                                response, exchange.getRequestPath(), acceptValue);

                        matched.put(response.getCode(), response);
                    }
                }
            }
        }

        LOGGER.trace("Matched {} candidate response definitions by Accept and Content-Type headers for resource: {}", matched.size(), endpoint.getUrl());
        return matched;
    }
}
