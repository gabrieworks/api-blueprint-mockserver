/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.service;

import com.gatehill.apibms.core.model.ExecutionInstance;
import com.gatehill.apibms.core.model.MockDefinition;
import com.gatehill.apibms.core.model.MockServer;
import com.gatehill.apibms.core.model.ResourceDefinition;
import com.gatehill.apibms.core.service.mockfactory.MockFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Tests for {@link com.gatehill.apibms.core.service.ExecutionServiceImpl}.
 *
 * @author pete
 */
public class ExecutionServiceTest {
    private static final int PORT = 8080;

    /**
     * Class under test.
     */
    @InjectMocks
    private ExecutionService service;

    @Mock
    private MockFactory mockFactory;

    @Mock
    private ServerService serverService;

    @Before
    public void before() {
        service = new ExecutionServiceImpl();

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testExecute() throws Exception {
        // test data
        final File jsonAst = new File(ExecutionServiceTest.class.getResource("/api1.json").getPath());

        final MockDefinition mock = new MockDefinition();
        mock.getEndpoints().add(new ResourceDefinition());

        final MockServer server = new MockServer(ServerService.HOST_LOCALHOST, PORT);

        // mock behaviour
        when(mockFactory.createMock(any(File.class), any(MockFactory.BlueprintFormat.class)))
                .thenReturn(mock);

        when(serverService.start(mock, ServerService.HOST_LOCALHOST, PORT))
                .thenReturn(server);

        // start server from blueprint
        final ExecutionInstance actual = service.execute(jsonAst, MockFactory.BlueprintFormat.AST_JSON, PORT);

        // assert
        Assert.assertNotNull(actual);
        Assert.assertEquals(PORT, actual.getPort());

        Assert.assertNotNull(actual.getEndpoints());
        Assert.assertEquals(1, actual.getEndpoints().length);

        // verify behaviour
        verify(mockFactory, times(1)).createMock(any(File.class), any(MockFactory.BlueprintFormat.class));
        verify(serverService, times(1)).start(mock, ServerService.HOST_LOCALHOST, PORT);
        verifyNoMoreInteractions(mockFactory, serverService);
    }
}
