# Make snowcrash for testing AST v2

This project depends on the **snowcrash** API Blueprint parser.
See the snowcrash project website at [http://github.com/apiaryio/snowcrash.git](http://github.com/apiaryio/snowcrash.git)

In order to use the mock server, follow these steps.

## Prerequisites
Install bundler:

    sudo gem install bundler

## Install
Run the following:

    git clone --recursive git://github.com/apiaryio/snowcrash.git
    cd snowcrash
    git checkout v0.15.0

Update submodules:

    git submodule update --init --recursive

Build and install:

    ./configure
    make install

## Run snowcrash
You should now be able to run the ``snowcrash`` command from your terminal, e.g.

    snowcrash -h
    
This should display the snowcrash help message. If you see something like this, then you're ready to go:

    usage: snowcrash [options] ... <input file>
    
    API Blueprint Parser

See the main README.md file for this project for next steps.
